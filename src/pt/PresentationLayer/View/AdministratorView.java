package pt.PresentationLayer.View;

import pt.BusinessLayer.BaseProduct;
import pt.BusinessLayer.CompositeProduct;
import pt.BusinessLayer.MenuItem;
import pt.BusinessLayer.Order;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class AdministratorView extends JFrame {
    private static final long serialVersionUID = 1L;

    private static final String TITLE = "Administrator View";
    private static final int WIDTH = 700;
    private static final int HEIGHT = 650;

    private final JLabel idItemLabel;
    private final JLabel tableLabel;
    private final JLabel priceLabel;
    private final JLabel idLabel;
    private final JLabel tipLabel;
    private final JLabel dateLabel;

    private final JButton create;
    private final JButton delete;
    private final JButton update;
    private final JButton exit;

    private JTextField idTextField = new JTextField();
    private JTextField tableTextField = new JTextField();
    private JTextField idItemTextField = new JTextField();
    private JTextField priceTextField = new JTextField();
    private JTextField tipTextField = new JTextField();
    private JTextField dateTextField = new JTextField();

    private final DefaultTableModel model;
    private final JTable  menuItemTable;

    private final JPanel left;
    private final JPanel right;
    private final JPanel buttons;

    public AdministratorView() throws HeadlessException {
        super(TITLE);
        idItemLabel = new JLabel("IdItem");
        tableLabel = new JLabel("Table");
        priceLabel = new JLabel("Price");
        idLabel = new JLabel("IdOrder");
        tipLabel = new JLabel("Type");
        dateLabel =new JLabel("Date");

        create = new JButton("Add MenuItem");
        delete = new JButton("Delete MenuItem");
        update = new JButton("Edit MenuItem");
        exit = new JButton("Exit");

        model = new DefaultTableModel();
        menuItemTable = new JTable(model);

        left = new JPanel();
        right = new JPanel();
        buttons = new JPanel();

        initializeView();
        setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView() {

        left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
        right.setLayout(new BoxLayout(right,BoxLayout.PAGE_AXIS));
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));


        left.add(Box.createRigidArea(new Dimension(20, 20)));
        JScrollPane jScrollPaneArticles = new JScrollPane(menuItemTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneArticles.setBorder(new EmptyBorder(10, 10, 10, 10));
        left.add(jScrollPaneArticles);

        add(left);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        idLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(idLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        idTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, idTextField.getPreferredSize().height) );
        right.add(idTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        idItemLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(idItemLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        idItemTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, dateTextField.getPreferredSize().height) );
        right.add(idItemTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        dateLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(dateLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        dateTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, dateTextField.getPreferredSize().height) );
        right.add(dateTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        tableLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(tableLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        tableTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, tableTextField.getPreferredSize().height) );
        right.add(tableTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        priceLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(priceLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        priceTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, priceTextField.getPreferredSize().height) );
        right.add(priceTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        tipLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(tipLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        tipTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, tipTextField.getPreferredSize().height) );
        right.add(tipTextField);

        add(right);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        create.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(create);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        update.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(update);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        delete.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(delete);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        exit.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(exit);

        add(buttons);

        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
    }

    //add action listeners
    public void addExitActionListener(ActionListener actionListener){ exit.addActionListener(actionListener);}
    public void addCreateActionListener(ActionListener actionListener){ create.addActionListener(actionListener);}
    public void addDeleteActionListener(ActionListener actionListener) { delete.addActionListener(actionListener);}
    public void addEditActionListener(ActionListener actionListener){ update.addActionListener(actionListener);}

    //get
    public String getDate(){ return dateTextField.getText(); }
    public String getTableTextField() { return tableTextField.getText(); }
    public String getIdTextField() { return idTextField.getText(); }
    public String getPriceTextField() { return priceTextField.getText(); }
    public String getTip() { return tipTextField.getText(); }
    public String getIdItemTextField(){ return idItemTextField.getText(); }

    private Integer getNumberOfMenuItems(HashMap<Order, LinkedList<pt.BusinessLayer.MenuItem>> map){
        Integer counter = new Integer(0);
        Iterator<Map.Entry<Order, LinkedList<pt.BusinessLayer.MenuItem>>> hashMapIterator = map.entrySet().iterator();

        while (hashMapIterator.hasNext()){
            Map.Entry pair = (Map.Entry) hashMapIterator.next();

            Order order = (Order) pair.getKey();
            LinkedList<pt.BusinessLayer.MenuItem> orderMenuItems = (LinkedList<pt.BusinessLayer.MenuItem>) pair.getValue();

            Iterator<pt.BusinessLayer.MenuItem> menuItemIterator = orderMenuItems.iterator();

            if(orderMenuItems.isEmpty()) counter++;

            while(menuItemIterator.hasNext()){
                pt.BusinessLayer.MenuItem menuItem = menuItemIterator.next();
                counter++;
            }
        }

        return counter;
    }

    public void createTable(HashMap<Order, LinkedList<pt.BusinessLayer.MenuItem>> hashmap){
        String[] col = {"IdOrder", "Date", "Table", "IdMenuItem", "TipMenuItem", "Pret" };
        Object[][] data = new Object[this.getNumberOfMenuItems(hashmap)][6];
        int i=0;

        Iterator<Map.Entry<Order, LinkedList<pt.BusinessLayer.MenuItem>>> hashMapIterator = hashmap.entrySet().iterator();

        while(hashMapIterator.hasNext()){
            Map.Entry pair = (Map.Entry) hashMapIterator.next();

            Order order = (Order) pair.getKey();
            LinkedList<pt.BusinessLayer.MenuItem> orderMenuItems = (LinkedList<pt.BusinessLayer.MenuItem>) pair.getValue();

            Iterator<pt.BusinessLayer.MenuItem> menuItemIterator = orderMenuItems.iterator();

            data[i][0] = order.getId();
            data[i][1] = order.getDate();
            data[i][2] = order.getTable();

            if(orderMenuItems.isEmpty()) i++;

            while(menuItemIterator.hasNext()){
                MenuItem menuItem = menuItemIterator.next();
                data[i][3] = menuItem.getIdMenuItem();

                if(menuItem instanceof BaseProduct) data[i][4] = "Base Product";
                if(menuItem instanceof CompositeProduct) data[i][4] = "Composite Product";

                data[i][5] = menuItem.getPrice();
                i++;
            }
        }

        model.setDataVector(data,col);


    }

    public void resetText(){
        dateTextField.setText("");
        tableTextField.setText("");
    }

}
