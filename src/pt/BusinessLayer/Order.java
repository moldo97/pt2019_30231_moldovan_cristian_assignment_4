package pt.BusinessLayer;

import java.io.Serializable;

public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    public Integer id;
    public String date;
    public Integer table;
    private static Integer idC = 0;

    public Order(String date , Integer table){
        this.date = date;
        this.table =table;
        this.id = new Integer(idC++);
    }

    public static void setStaticId(Integer id){ Order.idC = id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getId(){ return this.id; }

    public String getDate(){ return this.date; }

    public Integer getTable(){ return this.table; }
}
