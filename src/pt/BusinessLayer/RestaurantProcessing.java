package pt.BusinessLayer;

import java.util.LinkedList;

public interface RestaurantProcessing {

    //Administrator

    void createMenuItem(Order order ,MenuItem menuItem);

    void deleteMenuItem(Order order, Integer id);

    void editMenuItem(Order order, MenuItem menuItem);

    //Waiter

    void addNewOrder(Order order);

    void deleteOrder(Integer id);

    void computePrice(Order order);


}
