package pt.PresentationLayer.Controller;

import pt.BusinessLayer.Order;
import pt.BusinessLayer.Restaurant;
import pt.Main;
import pt.PresentationLayer.View.WaiterView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaiterController {
    private final WaiterView waiterView;
    private final Restaurant restaurant;

    public WaiterController(WaiterView waiterView,Restaurant restaurant){
        this.waiterView = waiterView;
        this.restaurant = restaurant;

        //add action listeners
        this.waiterView.addExitActionListener(new ExitActionListener());
        this.waiterView.addCreateActionListener(new CreateActionListener());
        this.waiterView.addDeleteActionListener(new DeleteActionListener());

        this.waiterView.createTable(restaurant.getHashMap());

        this.waiterView.setVisible(true);
    }

    private class ExitActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            waiterView.setVisible(false);
            Main.openInit();
        }
    }

    private class CreateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(waiterView.getDate().equals("")){
                writeMessage("Enter a date");
            }
            else if(waiterView.getTableTextField().equals("")){
                writeMessage("Enter a table");
            }
            else {
                restaurant.addNewOrder(new Order(waiterView.getDate(),Integer.parseInt(waiterView.getTableTextField())));
                waiterView.resetText();
                restaurant.writeOrders();
                restaurant.readOrders();
                waiterView.createTable(restaurant.getHashMap());
            }
        }
    }

    private class DeleteActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            restaurant.deleteOrder(waiterView.getSelectedRowId());
            restaurant.writeOrders();
            restaurant.readOrders();
            waiterView.createTable(restaurant.getHashMap());
        }
    }

    private void writeMessage(String mes){
        JOptionPane.showMessageDialog(null,mes);
    }
}
