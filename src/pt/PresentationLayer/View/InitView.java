package pt.PresentationLayer.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InitView extends JFrame {
    private static final long serialVersionUID = 1L;

    private static final String TITLE = "Initial View";
    private static final int WIDTH = 400;
    private static final int HEIGHT = 150;
    private JButton administrator = new JButton("Administrator");
    private JButton waiter = new JButton( "Waiter");
    private JButton chef = new JButton( " Chef");

    public InitView() throws HeadlessException {
        super(TITLE);
        setVisible(false);
        initializeView();
        setSize(WIDTH,HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView(){
        setLayout(null);
        administrator.setBounds(20,30,120,50);
        waiter.setBounds(150,30,80,50);
        chef.setBounds(260,30,80,50);

        add(administrator);
        add(waiter);
        add(chef);
    }

    public void addAdministratorLoginActionListener(ActionListener actionListener){
        administrator.addActionListener(actionListener);
    }

    public void addWaiterLoginActionListener(ActionListener actionListener){
        waiter.addActionListener(actionListener);
    }

    public void addChefLoginActionListener(ActionListener actionListener){
        chef.addActionListener(actionListener);
    }
}
