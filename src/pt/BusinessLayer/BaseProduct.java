package pt.BusinessLayer;

public class BaseProduct extends MenuItem{
    private static final long serialVersionUID = 1L;

    public BaseProduct(Double price) { super(price); }

    @Override
    public void newMenuItem(Order order) {

    }

    @Override
    public void computePrice() {

    }


}
