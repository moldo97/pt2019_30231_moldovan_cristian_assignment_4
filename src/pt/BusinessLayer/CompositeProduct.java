package pt.BusinessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem{
    private static final long serialVersionUID = 1L;

    public CompositeProduct(double price) { super(price); }

    @Override
    public void newMenuItem(Order order) {
        this.setChanged();
        this.notifyObservers("A fost creat un menuItem (CompositeProduct)");
    }

    @Override
    public void computePrice() {

    }
}
