package pt.PresentationLayer.View;

import pt.BusinessLayer.BaseProduct;
import pt.BusinessLayer.CompositeProduct;
import pt.BusinessLayer.MenuItem;
import pt.BusinessLayer.Order;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class WaiterView extends JFrame {
    private static final long serialVersionUID = 1L;

    private static final String TITLE = "Waiter View";
    private static final int WIDTH = 700;
    private static final int HEIGHT = 600;

    private final JLabel dateLabel;
    private final JLabel tableLabel;

    private final JButton create;
    private final JButton exit;
    private final JButton delete;

    private JTextField tableTextField = new JTextField();
    private JTextField dateTextField = new JTextField();

    private final DefaultTableModel model;
    private final JTable  ordersTable;

    private final JPanel left;
    private final JPanel right;
    private final JPanel buttons;

    public WaiterView() throws HeadlessException {
    super(TITLE);
    dateLabel = new JLabel("Date");
    tableLabel = new JLabel("Table");

    create = new JButton("Add New Order");
    delete = new JButton("Delete orders");
    exit = new JButton("Exit");

    model = new DefaultTableModel();
    ordersTable = new JTable(model);

    left = new JPanel();
    right = new JPanel();
    buttons = new JPanel();

    initializeView();
    setVisible(false);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
}

    private void initializeView() {

        left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
        right.setLayout(new BoxLayout(right,BoxLayout.PAGE_AXIS));
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));


        left.add(Box.createRigidArea(new Dimension(20, 20)));
        JScrollPane jScrollPaneArticles = new JScrollPane(ordersTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneArticles.setBorder(new EmptyBorder(10, 10, 10, 10));
        left.add(jScrollPaneArticles);

        add(left);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        dateLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(dateLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        dateTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, dateTextField.getPreferredSize().height) );
        right.add(dateTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        tableLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(tableLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        tableTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, tableTextField.getPreferredSize().height) );
        right.add(tableTextField);

        add(right);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        create.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(create);


        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        exit.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(exit);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        delete.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(delete);


        add(buttons);

        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
    }

    //add action listeners
    public void addExitActionListener(ActionListener actionListener){ exit.addActionListener(actionListener);}
    public void addCreateActionListener(ActionListener actionListener) { create.addActionListener(actionListener);}
    public void addDeleteActionListener(ActionListener actionListener) { delete.addActionListener(actionListener);}

    //get
    public String getDate() { return dateTextField.getText(); }
    public String getTableTextField() { return tableTextField.getText(); }

    private Integer getNumberOfMenuItems(HashMap<Order, LinkedList<MenuItem>> map){
        Integer counter = new Integer(0);
        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = map.entrySet().iterator();

        while (hashMapIterator.hasNext()){
            Map.Entry pair = (Map.Entry) hashMapIterator.next();

            Order order = (Order) pair.getKey();
            LinkedList<MenuItem> orderMenuItems = (LinkedList<MenuItem>) pair.getValue();

            Iterator<MenuItem> menuItemIterator = orderMenuItems.iterator();

            if(orderMenuItems.isEmpty()) counter++;

            while(menuItemIterator.hasNext()){
                MenuItem menuItem = menuItemIterator.next();
                counter++;
            }
        }

        return counter;
    }

    public void createTable(HashMap<Order, LinkedList<MenuItem>> hashmap){
        String[] col = {"Id", "Date", "Table", "IdMenuItem", "TipMenuItem", "Pret" };
        Object[][] data = new Object[this.getNumberOfMenuItems(hashmap)][6];
        int i=0;

        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = hashmap.entrySet().iterator();

        while(hashMapIterator.hasNext()){
            Map.Entry pair = (Map.Entry) hashMapIterator.next();

            Order order = (Order) pair.getKey();
            LinkedList<MenuItem> orderMenuItems = (LinkedList<MenuItem>) pair.getValue();

            Iterator<MenuItem> menuItemIterator = orderMenuItems.iterator();

            data[i][0] = order.getId();
            data[i][1] = order.getDate();
            data[i][2] = order.getTable();

            if(orderMenuItems.isEmpty()) i++;

            while(menuItemIterator.hasNext()){
                MenuItem menuItem = menuItemIterator.next();
                data[i][3] = menuItem.getIdMenuItem();

                if(menuItem instanceof BaseProduct) data[i][4] = "Base Product";
                if(menuItem instanceof CompositeProduct) data[i][4] = "Composite Product";

                data[i][5] = menuItem.getPrice();
                i++;
            }
        }

        model.setDataVector(data,col);

        ordersTable.getColumnModel().getColumn(0).setPreferredWidth(1);

    }

    public Integer getSelectedRowId() {
        return (Integer) ordersTable.getValueAt(ordersTable.getSelectedRow(),0);
    }
    public String getSelectedRowDate() {
        return (String) ordersTable.getValueAt(ordersTable.getSelectedRow(),1);
    }

    public Integer getSelectedRowTable(){
        return (Integer) ordersTable.getValueAt(ordersTable.getSelectedRow(),2);
    }
    public void resetText(){
        dateTextField.setText("");
        tableTextField.setText("");
    }
}
