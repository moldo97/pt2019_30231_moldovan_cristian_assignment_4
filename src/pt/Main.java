package pt;

import pt.BusinessLayer.Restaurant;
import pt.PresentationLayer.Controller.AdministratorController;
import pt.PresentationLayer.Controller.ChefController;
import pt.PresentationLayer.Controller.InitController;
import pt.PresentationLayer.Controller.WaiterController;
import pt.PresentationLayer.View.AdministratorView;
import pt.PresentationLayer.View.ChefView;
import pt.PresentationLayer.View.InitView;
import pt.PresentationLayer.View.WaiterView;

import java.awt.geom.RectangularShape;

public class Main {

    private static InitView initView;
    private static AdministratorView administratorView;
    private static WaiterView waiterView;
    private static ChefView chefView;

    private static InitController initController;
    private static AdministratorController administratorController;
    private static WaiterController waiterController;
    private static ChefController chefController;

    private static Restaurant restaurant;

    public static void main(String[] args) {

        //initialize view
        initView = new InitView();
        administratorView = new AdministratorView();
        waiterView = new WaiterView();
        chefView = new ChefView();

        restaurant = new Restaurant();


        openInit();
    }

    public static void openInit(){
        initController = new InitController(initView);

    }

    public static void openAdministratorView(){
        administratorController = new AdministratorController(administratorView,restaurant);
    }

    public static void openWaiterView(){
        waiterController = new WaiterController(waiterView,restaurant);
    }

    public static void openChefView(){
        chefController = new ChefController(chefView);
        restaurant.addObserver(chefController);
    }
}
