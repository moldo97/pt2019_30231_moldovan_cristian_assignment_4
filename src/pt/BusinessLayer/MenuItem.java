package pt.BusinessLayer;

import java.io.Serializable;
import java.util.Observable;

public abstract class MenuItem extends Observable implements Serializable {
    private static final long serialVersionUID = 1L;

    private static Integer id = 0;
    private Integer idMenuItem;
    private Double price;

    public MenuItem(Double price){
        idMenuItem = new Integer(MenuItem.id++);
        this.price = price;
    }

    public static void setStaticId(Integer id) {
        MenuItem.id = id;
    }

    public Integer getIdMenuItem() {
        return this.idMenuItem;
    }


    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public abstract void newMenuItem(Order order);

    public abstract void computePrice();
}
