package pt.PresentationLayer.Controller;

import pt.BusinessLayer.Restaurant;
import pt.Main;
import pt.PresentationLayer.View.ChefView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class ChefController implements Observer {
    private final ChefView chefView;

    public ChefController(ChefView chefView){
        this.chefView = chefView;

        //add action listeners
        this.chefView.addExitActionListener(new ExitActionListener());


        this.chefView.setVisible(true);


    }

    @Override
    public void update(Observable o, Object arg) {

    }

    private class ExitActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            chefView.setVisible(false);
            Main.openInit();
        }
    }
}
