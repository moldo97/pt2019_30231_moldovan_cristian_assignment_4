package pt.PresentationLayer.Controller;

import pt.BusinessLayer.*;
import pt.Main;
import pt.PresentationLayer.View.AdministratorView;
import pt.PresentationLayer.View.WaiterView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdministratorController {
    private final AdministratorView administratorView;
    private final Restaurant restaurant;

    public AdministratorController(AdministratorView administratorView, Restaurant restaurant){
        this.administratorView = administratorView;
        this.restaurant =restaurant;

        //add action listeners
        this.administratorView.addExitActionListener(new ExitActionListener());
        this.administratorView.addCreateActionListener(new CreateActionListener());
        this.administratorView.addDeleteActionListener(new DeleteActionListener());
        this.administratorView.addEditActionListener(new UpdateActionListener());

        this.administratorView.createTable(restaurant.getHashMap());

        this.administratorView.setVisible(true);
    }

    private class ExitActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            administratorView.setVisible(false);
            Main.openInit();
        }
    }

    private class CreateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(administratorView.getIdTextField().equals("")){
                writeMessage("Enter a order id");
            }
            else if(administratorView.getDate().equals("")){
                writeMessage("Enter a date");
            }
            else if(administratorView.getTableTextField().equals("")){
                writeMessage("Enter a table");
            }
            else if(administratorView.getTip().equals("")){
                writeMessage("Enter a type");
            }
            else if(administratorView.getPriceTextField().equals("")){
                writeMessage("Enter a price");
            }
            else{
                if(administratorView.getTip().equals("Base Product")){
                    MenuItem menuItem= new BaseProduct(Double.parseDouble(administratorView.getPriceTextField()));
                    Order order = new Order(administratorView.getDate(),Integer.parseInt(administratorView.getTableTextField()));
                    order.setId(Integer.parseInt(administratorView.getIdTextField()));
                    restaurant.createMenuItem(order,menuItem);
                }
                else if(administratorView.getTip().equals("Composite Product")){
                    MenuItem menuItem = new CompositeProduct(Double.parseDouble(administratorView.getPriceTextField()));
                    Order order = new Order(administratorView.getDate(),Integer.parseInt(administratorView.getTableTextField()));
                    order.setId(Integer.parseInt(administratorView.getIdTextField()));
                    restaurant.createMenuItem(order,menuItem);
                }
            }


            restaurant.writeOrders();
            restaurant.readOrders();
            administratorView.createTable(restaurant.getHashMap());
        }
    }

    private class DeleteActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(administratorView.getIdTextField().equals("")){
                writeMessage("Enter an order id");
            }
            else if(administratorView.getIdItemTextField().equals("")){
                writeMessage("Enter an item id");
            }
            else if(administratorView.getDate().equals("")){
                writeMessage("Enter a date");
            }
            else if(administratorView.getTableTextField().equals("")){
                writeMessage("Enter a table");
            }
            else{
                Order order = new Order(administratorView.getDate(),Integer.parseInt(administratorView.getTableTextField()));
                order.setId(Integer.parseInt(administratorView.getIdTextField()));

                restaurant.deleteMenuItem(order,Integer.parseInt(administratorView.getIdItemTextField()));
                restaurant.writeOrders();
                administratorView.createTable(restaurant.getHashMap());
            }
        }
    }

    private class UpdateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }

    private void writeMessage(String mes){
        JOptionPane.showMessageDialog(null,mes);
    }
}
