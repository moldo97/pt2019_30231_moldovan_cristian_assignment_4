package pt.PresentationLayer.Controller;

import pt.Main;
import pt.PresentationLayer.View.InitView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InitController {
    private final InitView initView;

    public InitController(InitView initView){
        this.initView = initView;

        //add action listeners
        this.initView.addAdministratorLoginActionListener(new AdministratorActionListener());
        this.initView.addWaiterLoginActionListener(new WaiterActionListener());
        this.initView.addChefLoginActionListener(new ChefActionListener());

        this.initView.setVisible(true);
    }

    private class AdministratorActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Main.openAdministratorView();
        }
    }

    private class WaiterActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Main.openWaiterView();
        }
    }

    private class ChefActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Main.openChefView();
        }
    }

}
