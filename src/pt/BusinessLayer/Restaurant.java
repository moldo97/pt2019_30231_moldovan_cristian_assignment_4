package pt.BusinessLayer;


import java.io.*;
import java.util.*;

public class Restaurant extends Observable implements RestaurantProcessing, Serializable {
    private HashMap<Order, LinkedList<MenuItem>> map;
    private static Integer id;

    public Restaurant(){
        this.map = this.readOrders();
        this.readOrders();
        this.setStaticId();
        id = new Integer(0);

    }

    public HashMap<Order, LinkedList<MenuItem>> getHashMap(){
        return this.map;
    }

    public void setStaticId(){
        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = map.entrySet().iterator();
        Integer max = new Integer(0);
        Integer maxO = new Integer(0);

        while (hashMapIterator.hasNext()){
            Map.Entry pair = (Map.Entry)hashMapIterator.next();

            Order order = (Order)pair.getKey();
            LinkedList<MenuItem> orderMenuItems = (LinkedList<MenuItem>) pair.getValue();

            if (order.getId() > maxO){
                maxO = order.getId();
            }

            Iterator<MenuItem> menuItemIterator = orderMenuItems.iterator();

            while (menuItemIterator.hasNext()){
                MenuItem menuItem = menuItemIterator.next();
                if ( menuItem.getIdMenuItem() > max){
                    max = menuItem.getIdMenuItem();
                }
            }
        }

        Order.setStaticId(maxO);
        MenuItem.setStaticId(max);

    }

    @Override
    public void createMenuItem(Order order,MenuItem menuItem) {
        assert order != null : "order == null";
        assert menuItem != null : "menuItem == null";

        int orderPreSize = map.size();
        int menuItemPreSize = 0;
        int menuItemPostSize = 0;

        int ok=1;
        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = map.entrySet().iterator();

        while (hashMapIterator.hasNext() && ok==1){
            Map.Entry pair = (Map.Entry) hashMapIterator.next();
            Order auxOrder = (Order) pair.getKey();

            if(auxOrder.getId().equals(order.getId())){
                menuItemPreSize = this.getOrderMenuItems(auxOrder).size();

                ok = 0;
                menuItem.newMenuItem(auxOrder);
                map.get(auxOrder).add(menuItem);

                menuItemPostSize = this.getOrderMenuItems(auxOrder).size();
            }
        }

        int orderPostSize = map.size();

        assert menuItemPreSize != menuItemPostSize +1 : "MenuItem.presize() == Menuitem.postsize()";
        assert orderPreSize == orderPostSize : "Order.presize() != Order.postsize()";
        assert !map.containsValue(menuItem) : order.getId() + "nu s-a adaugat";
    }

    public LinkedList<MenuItem> getOrderMenuItems(Order order){
        LinkedList<MenuItem> menuItems = new LinkedList<>();
        Integer id = order.getId();

        int ok=1;
        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = map.entrySet().iterator();

        while(hashMapIterator.hasNext() && ok == 1){
            Map.Entry pair = (Map.Entry)hashMapIterator.next();
            Order auxOrder = (Order)pair.getKey();

            if (order.getId().equals(auxOrder.getId())){
                menuItems = (LinkedList<MenuItem>) pair.getValue();
            }
        }

        return menuItems;
    }

    @Override
    public void deleteMenuItem(Order order, Integer id) {
        assert id != null : "id == null";
        assert order != null : "order == null";

        int menuItemPreSize = 0;
        int menuItemPostSize = 0;

        int orderPreSize = map.size();

        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = map.entrySet().iterator();
        MenuItem menuItem = null;
        int ok=1;

        while(ok == 1 && hashMapIterator.hasNext()){
            Map.Entry pair =  hashMapIterator.next();
            Order auxOrder = (Order) pair.getKey();
            LinkedList<MenuItem> menuItems = (LinkedList<MenuItem>) pair.getValue();
            Iterator<MenuItem> menuItemIterator = menuItems.iterator();

            while(menuItemIterator.hasNext() && ok == 1){
                menuItem = menuItemIterator.next();

                if(menuItem.getIdMenuItem().equals(id)){
                    menuItemPreSize = map.get(auxOrder).size();

                    ok=0;
                    menuItems.remove(menuItem);

                    menuItemPostSize = map.get(auxOrder).size();
                }
            }
        }

        int orderPostSize = map.size();

        assert menuItemPreSize != menuItemPostSize - 1 : "menuItem.preSize() == menuItem.postSize()";
        assert orderPreSize == orderPostSize : "order.preSize() != order.postSize()";
        assert !this.map.containsKey(order) : order.getDate() + " nu exista";
        assert !this.map.containsValue(menuItem) : menuItem.getIdMenuItem() + " nu s-a sters";
    }

    @Override
    public void editMenuItem(Order order, MenuItem menuItem) {

    }

    @Override
    public void addNewOrder(Order order) {
        assert order != null : "order == null";

        int preSize = map.size();

        map.put(order, new LinkedList<MenuItem>());
        this.setChanged();
        this.notifyObservers("Comanda " + order.getId() + " a fost plasata la " + order.getDate() + " de la masa " + order.getTable());

        int postSize = map.size();

        assert preSize != 1 + postSize : "preSize() == postSize()";
        assert this.map.containsKey(order) : order.getDate() + "exista";
    }

    @Override
    public void deleteOrder(Integer id) {
        assert id != null : "id == null";

        int preSize = map.size();
        int ok = 1;

        Iterator<Map.Entry<Order, LinkedList<MenuItem>>> hashMapIterator = map.entrySet().iterator();

        while(hashMapIterator.hasNext() && ok==1){
            Map.Entry pair = (Map.Entry) hashMapIterator.next();
            Order auxOrder = (Order) pair.getKey();

            if(auxOrder.getId().equals(id)){
                ok = 0;
                map.remove(auxOrder);
            }
        }

        int postSize = map.size();

        assert preSize !=postSize-1 : "preSize() == postSize()";
    }

    @Override
    public void computePrice(Order order) {

    }


    public HashMap<Order, LinkedList<MenuItem>> readOrders(){
        FileInputStream fileIn = null;
        ObjectInputStream obj = null;
        HashMap<Order,LinkedList<MenuItem>> auxMap = new HashMap<>();

        try {
            fileIn = new FileInputStream("D:\\An_3\\Semestrul_2\\TP_Restanta\\Tema4\\Orders\\order.ser");
            obj = new ObjectInputStream(fileIn);

            auxMap = (HashMap<Order,LinkedList<MenuItem>>) obj.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileIn.close();
                obj.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return auxMap;
    }

    public void writeOrders(){
        FileOutputStream fileOut = null;
        ObjectOutputStream obj = null;

        try {
            fileOut = new FileOutputStream("D:\\An_3\\Semestrul_2\\TP_Restanta\\Tema4\\Orders\\order.ser");
            obj = new ObjectOutputStream(fileOut);

            obj.writeObject(map);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOut.close();
                obj.close();
            } catch (Exception e) {
            }
        }
    }
}
