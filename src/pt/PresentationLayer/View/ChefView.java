package pt.PresentationLayer.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class ChefView extends JFrame{
    private static final String TITLE = "Chef View";
    private static final int WIDTH = 250;
    private static final int HEIGHT = 250;

    private static final JButton exit = new JButton("Exit");

    public ChefView() throws HeadlessException {
        super(TITLE);
        setVisible(false);
        setSize(WIDTH,HEIGHT);
        initializeView();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView(){
        setLayout(null);

        exit.setBounds(50,50,100,50);

        add(exit);
    }

    //add action listeners
    public void addExitActionListener(ActionListener actionListener){ exit.addActionListener(actionListener);}

}
